#!/usr/bin/perl

use strict;

my $cmd = `cat /proc/cpuinfo | grep 'model name' | uniq`;
chomp $cmd;

$cmd =~ s/.*?:\s+([AMD|Intel].*?)$/$1/;
$cmd =~ s/Processor//;
$cmd =~ s/\(tm\)//;

print "$cmd\n";
