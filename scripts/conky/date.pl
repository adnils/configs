#!/usr/bin/perl

use strict;

use POSIX qw(strftime);

my @months = (
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
);

my %weekday = (
  '0' => 'Sunday',
  '1' => 'Monday',
  '2' => 'Tuesday',
  '3' => 'Wednesday',
  '4' => 'Thursday',
  '5' => 'Friday',
  '6' => 'Saturday'
);

my @date = localtime(time);
SWITCH: {
  ($date[3] == 1)     && do { $date[3] .= 'st'; last SWITCH; };
  ($date[3] =~ m/^1/) && do { $date[3] .= 'th'; last SWITCH; };
  ($date[3] =~ m/1$/) && do { $date[3] .= 'st'; last SWITCH; };
  ($date[3] =~ m/2$/) && do { $date[3] .= 'nd'; last SWITCH; };
  ($date[3] =~ m/3$/) && do { $date[3] .= 'rd'; last SWITCH; };
  $date[3] .= 'th';
};

print "$weekday{$date[6]} ~ $months[$date[4]] $date[3], " . ($date[5]+1900) . " ~";
