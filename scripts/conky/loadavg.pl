#!/usr/bin/perl

use strict;
# 01:18:43 up  3:13,  3 users,  load average: 0.00, 0.01, 0.05

my $cmd;

$cmd = `uptime`;
chomp $cmd;

$cmd =~ s/^.*?\w:\s+(\d+\.\d+).*?$/$1/;

print "$cmd\n";
